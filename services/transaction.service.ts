import { GetTransactionsOptions } from '@/types';
import axiosClient from './axios-client';

export const transactionService = {
  getTransactions: async (params: GetTransactionsOptions) =>
    (
      await axiosClient.get(
        `/transactions?page=${params.page}&size=${params.size}&sortBy=${params.sortBy}&sortOrder=${params.sortOrder}&keyword=${params.keyword}&value=${params.value}`
      )
    ).data,

  getTransactionDetail: async (transactionId: string, userId: string) =>
    (await axiosClient.get(`/transactions/${transactionId}?userId=${userId}`))
      .data,

  updateTransactionStatus: async (
    transactionId: string,
    status: number,
    userId: string
  ) =>
    (
      await axiosClient.put(`/transactions/${transactionId}`, {
        data: {
          status: status,
          userId: userId
        }
      })
    ).data
};
