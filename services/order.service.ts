import { GetOrdersResponse } from '@/types';
import axiosClient from './axios-client';

export const orderService = {
  getOrders: async (params: GetOrdersResponse) =>
    (
      await axiosClient.get(
        `/orders?page=${params.page}&size=${params.size}&sortBy=${params.sortBy}&sortOrder=${params.sortOrder}&status=${params.status}`
      )
    ).data,

  getOrderDetail: async (id: string) =>
    (await axiosClient.get(`/orders/${id}`)).data,
  updateOrderStatus: async (id: string, status: number, userId: string) =>
    (
      await axiosClient.put(`/orders/${id}`, {
        data: {
          status: status,
          userId: userId
        }
      })
    ).data
};
