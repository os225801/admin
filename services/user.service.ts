import axiosClient from './axios-client';

export const userService = {
  getProfile: async (params: { userId: string }) =>
    (await axiosClient.get(`/users/${params.userId}`)).data,
  getLoggingUser: async (params: {
    page?: number;
    size?: number;
    sortBy?: string;
    sortOrder?: string;
  }) =>
    (
      await axiosClient.get(
        `/users?page=${params.page}&size=${params.size}&sortBy=${params.sortBy}&sortOrder=${params.sortOrder}`
      )
    ).data
};
