import { Icons } from '@/components/icons';

export interface GetTransactionsOptions {
  page: number;
  size: number;
  sortBy: string;
  sortOrder: string;
  keyword: string;
  value: string;
}

export interface GetOrdersResponse {
  page: number;
  size: number;
  sortBy: string;
  sortOrder: string;
  status: number;
}
export interface NavItem {
  title: string;
  href?: string;
  disabled?: boolean;
  external?: boolean;
  icon?: keyof typeof Icons;
  label?: string;
  description?: string;
}

export interface NavItemWithChildren extends NavItem {
  items: NavItemWithChildren[];
}

export interface NavItemWithOptionalChildren extends NavItem {
  items?: NavItemWithChildren[];
}

export interface FooterItem {
  title: string;
  items: {
    title: string;
    href: string;
    external?: boolean;
  }[];
}

export type MainNavItem = NavItemWithOptionalChildren;

export type SidebarNavItem = NavItemWithChildren;
