import { useToast } from '@/components/ui/use-toast';
import { orderService } from '@/services/order.service';
import { keepPreviousData, useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { useRouter } from 'next/navigation';

const fetchOrderPaginated = async ({
  page = 1,
  size = 10,
  sortBy = 'status',
  sortOrder = 'DESC',
  status = 1
}) => {
  const transactions = await orderService.getOrders({
    page,
    size,
    sortBy,
    sortOrder,
    status
  });
  return transactions;
};

export const useOrderPaginated = ({
  page = 1,
  size = 10,
  sortBy = 'status',
  sortOrder = 'DESC',
  status = 1
}) => {
  const { isPending, isError, error, data, isFetching, isPlaceholderData } =
    useQuery({
      queryKey: ['orders', page, size, sortBy, sortOrder, status],
      queryFn: () =>
        fetchOrderPaginated({
          page,
          size,
          sortBy,
          sortOrder,
          status
        }),
      placeholderData: keepPreviousData
    });

  return {
    isPending,
    isError,
    error,
    data,
    isFetching,
    isPlaceholderData
  };
};

export const useOrderDetail = (id: string) => {
  const { isPending, isError, error, data, isFetching, isPlaceholderData } =
    useQuery({
      queryKey: ['order', id],
      queryFn: () => orderService.getOrderDetail(id),
      placeholderData: keepPreviousData
    });

  return {
    isPending,
    isError,
    error,
    data,
    isFetching,
    isPlaceholderData
  };
};

const updateOrderStatus = async ({
  orderId,
  status,
  userId
}: {
  orderId: string;
  status: number;
  userId: string;
}) => {
  try {
    const updatedOrder = await orderService.updateOrderStatus(
      orderId,
      status,
      userId
    );
    return updatedOrder;
  } catch (error) {
    console.log((error as any).response.data?.resultMessage);
    throw new Error((error as any).response.data?.resultMessage);
  }
};

export const useUpdateOrderStatus = () => {
  const router = useRouter();
  const queryClient = useQueryClient();
  const { toast } = useToast();
  const { mutateAsync: updateOrder } = useMutation({
    mutationFn: updateOrderStatus,
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ['orders'] });
      queryClient.invalidateQueries({ queryKey: ['transactions'] });
      toast({
        variant: 'default',
        title: 'Order updated.',
        description: 'Order status has been updated.'
      });
      router.back();
    },
    onError: (error) => {
      toast({
        variant: 'destructive',
        title: 'Uh oh! Something went wrong.',
        description: error.message
      });
    }
  });

  return {
    updateOrder
  };
};
