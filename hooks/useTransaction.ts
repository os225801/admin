import { useToast } from '@/components/ui/use-toast';
import { transactionService } from '@/services/transaction.service';
import {
  keepPreviousData,
  useMutation,
  useQuery,
  useQueryClient
} from '@tanstack/react-query';
import { useRouter } from 'next/navigation';

const fetchTransactionPaginated = async ({
  page = 1,
  size = 10,
  sortBy = 'transactionDate',
  sortOrder = 'DESC',
  keyword = 'type',
  value = '1'
}) => {
  const transactions = await transactionService.getTransactions({
    page,
    size,
    sortBy,
    sortOrder,
    keyword,
    value
  });
  return transactions;
};

export const useTransactionPaginated = ({
  page = 1,
  size = 10,
  sortBy = 'transactionDate',
  sortOrder = 'DESC',
  keyword = 'type',
  value = '1'
}) => {
  const { isPending, isError, error, data, isFetching, isPlaceholderData } =
    useQuery({
      queryKey: ['transactions', page, size, sortBy, sortOrder, keyword, value],
      queryFn: () =>
        fetchTransactionPaginated({
          page,
          size,
          sortBy,
          sortOrder,
          keyword,
          value
        }),
      placeholderData: keepPreviousData
    });

  return {
    isPending,
    isError,
    error,
    data,
    isFetching,
    isPlaceholderData
  };
};

export const useTransactionDetail = (transactionId: string, userId: string) => {
  const { isError, error, data } = useQuery({
    queryKey: ['transactionDetail', transactionId, userId],
    queryFn: () =>
      transactionService.getTransactionDetail(transactionId, userId)
  });

  return {
    isError,
    error,
    data
  };
};

const updateTransactionStatus = async ({
  transactionId,
  status,
  userId
}: {
  transactionId: string;
  status: number;
  userId: string;
}) => {
  try {
    const updatedTransaction = await transactionService.updateTransactionStatus(
      transactionId,
      status,
      userId
    );
    return updatedTransaction;
  } catch (error) {
    console.log((error as any).response.data?.resultMessage);
    throw new Error((error as any).response.data?.resultMessage);
  }
};

export const useUpdateTransactionStatus = () => {
  const router = useRouter();
  const queryClient = useQueryClient();
  const { toast } = useToast();
  const { mutateAsync: updateTransaction } = useMutation({
    mutationFn: updateTransactionStatus,
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ['transactions'] });
      toast({
        variant: 'default',
        title: 'Transaction updated.',
        description: 'Transaction status has been updated.'
      });
      router.back();
    },
    onError: (error) => {
      toast({
        variant: 'destructive',
        title: 'Uh oh! Something went wrong.',
        description: error.message
      });
    }
  });

  return {
    updateTransaction
  };
};
