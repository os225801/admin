import { userService } from '@/services/user.service';
import { keepPreviousData, useQuery } from '@tanstack/react-query';

export const useProfile = ({ params }: { params: { id: string } }) => {
  const { data, isLoading, isError } = useQuery({
    queryFn: async () => {
      const user = await userService.getProfile({ userId: String(params.id) }); // Pass an object with the userId property
      return user;
    },
    queryKey: ['user', params.id]
  });

  return { data, isLoading, isError } as {
    data: any;
    isLoading: boolean;
    isError: boolean;
  };
};

const fetchLoggingUser = async ({
  page = 1,
  size = 10,
  sortBy = 'expiryDate',
  sortOrder = 'DESC'
}) => {
  const transactions = await userService.getLoggingUser({
    page,
    size,
    sortBy,
    sortOrder
  });
  return transactions;
};

export const useLoggingUser = ({
  page = 1,
  size = 10,
  sortBy = 'expiryDate',
  sortOrder = 'DESC'
}) => {
  const { isPending, isError, error, data, isFetching, isPlaceholderData } =
    useQuery({
      queryKey: ['loggingUser', page, size, sortBy, sortOrder],
      queryFn: () =>
        fetchLoggingUser({
          page,
          size,
          sortBy,
          sortOrder
        }),
      placeholderData: keepPreviousData
    });

  return {
    isPending,
    isError,
    error,
    data,
    isFetching,
    isPlaceholderData
  };
};
