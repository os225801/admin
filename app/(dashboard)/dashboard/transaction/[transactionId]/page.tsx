import BreadCrumb from '@/components/breadcrumb';
import TransactionDetail from '@/components/transaction/transaction-detail';
import { ScrollArea } from '@/components/ui/scroll-area';

export default function Page() {
  const breadcrumbItems = [
    { title: 'Transaction', link: '/dashboard/transaction' },
    { title: 'Detail', link: '/dashboard/transaction/detail' }
  ];
  return (
    <ScrollArea className="h-full">
      <div className="flex-1 space-y-4 p-5">
        <BreadCrumb items={breadcrumbItems} />
        <TransactionDetail />
      </div>
    </ScrollArea>
  );
}
