import Transaction from '@/components/transaction/transaction-screen';

type paramsProps = {
  searchParams: {
    [key: string]: string | string[] | undefined;
  };
};

export default function page({ searchParams }: paramsProps) {
  return (
    // <div className="flex-1 space-y-4  p-4 pt-6 md:p-8">
    //   <BreadCrumb items={breadcrumbItems} />
    //   <UserClient/>
    // </div>
    <div>
      <Transaction searchParams={searchParams} />
    </div>
  );
}
