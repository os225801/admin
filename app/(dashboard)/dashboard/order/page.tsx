import Order from '@/components/order/order-screen';


type paramsProps = {
  searchParams: {
    [key: string]: string | string[] | undefined;
  };
};

const page = ({ searchParams }: paramsProps) => {
  return (
    <div>
      <Order searchParams={searchParams} />
    </div>
  )
}

export default page