import BreadCrumb from '@/components/breadcrumb';
import OrderDetail from '@/components/order/order-detail';

export default function Page() {
  const breadcrumbItems = [
    { title: 'Order', link: '/dashboard/order' },
    { title: 'Detail', link: '/dashboard/order/detail' }
  ];
  return (
    <div className="flex-1 space-y-4 p-8 ">
      <BreadCrumb items={breadcrumbItems} />
      <OrderDetail />
    </div>
  );
}
