import User from "@/components/layout/user/user-screen";


type paramsProps = {
  searchParams: {
    [key: string]: string | string[] | undefined;
  };
};

export default function page({ searchParams }: paramsProps) {
  return (
    <div>
      <User searchParams={searchParams} />
    </div>
  );
}