'use client';

import { create } from 'zustand';

interface TransactionStore {
  userId: string;
  buyerId: string;
  sellerId: string;
  actions: TransactionStoreActions;
}

interface TransactionStoreActions {
  // eslint-disable-next-line no-unused-vars
  setUserId: (id: string) => void;
  // eslint-disable-next-line no-unused-vars
  setBuyerId: (id: string) => void;
  // eslint-disable-next-line no-unused-vars
  setSellerId: (id: string) => void;
}

const useTransactionStore = create<TransactionStore>((set) => ({
  userId: '',
  buyerId: '',
  sellerId: '',
  actions: {
    setUserId: (userId: string) => set({ userId }),
    setBuyerId: (buyerId: string) => set({ buyerId }),
    setSellerId: (sellerId: string) => set({ sellerId })
  }
}));

export const useTransactionStoreActions = () =>
  useTransactionStore((state) => state.actions);
export const useUserId = () => useTransactionStore((state) => state.userId);
export const useBuyerId = () => useTransactionStore((state) => state.buyerId);
export const useSellerId = () => useTransactionStore((state) => state.sellerId);
