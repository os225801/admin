export const isClient = () => {
  return (
    typeof window !== 'undefined' &&
    typeof window.sessionStorage !== 'undefined'
  );
};

export const getSession = () => {
  if (isClient()) {
    const userId = sessionStorage.getItem('userId');
    const role = sessionStorage.getItem('role');
    const userToken = sessionStorage.getItem('userToken');

    return { userId, role, userToken };
  }
  return { userId: null, role: null, userToken: null };
};
