import { type ClassValue, clsx } from 'clsx';
import { twMerge } from 'tailwind-merge';

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}

export function formatToVND(amount: number) {
  if (typeof amount !== 'number' || isNaN(amount)) {
    return '0';
  }
  return amount.toLocaleString('vi-VN', { style: 'currency', currency: 'VND' });
}

export function formatDate(isoDate : string) {
  const date = new Date(isoDate);
  if (isNaN(date as any)) {
    return '';
  }

  const options = {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
  };

  return date.toLocaleDateString('vi-VN', options as any);
}
