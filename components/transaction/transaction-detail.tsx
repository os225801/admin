'use client'
import { transactionStatus, transactionType } from "@/constants/data"
import { useProfile } from "@/hooks/useProfile"
import { useTransactionDetail, useUpdateTransactionStatus } from "@/hooks/useTransaction"
import { useUserId } from "@/lib/store"
import { formatToVND } from "@/lib/utils"
import { useParams } from "next/navigation"
import { Button } from "../ui/button"

const TransactionDetail = () => {
  const { transactionId } = useParams()
  const userId = useUserId()
  const { data } = useTransactionDetail(String(transactionId), userId)
  const { updateTransaction } = useUpdateTransactionStatus()
  const { data: user } = useProfile({ params: { id: userId } })
  const handleUpdateStatus = async (status: number) => {
    await updateTransaction({ transactionId: String(transactionId), status, userId })
  }

  // Convert transactionDate to a readable format
  const formattedDate = new Date(data?.transactionDate).toLocaleString();



  return (
    <div>
      <div className="border border-gray-300 rounded-lg p-6 w-full mx-auto mt-10 font-sans">
        <h2 className="text-2xl font-bold text-center text-gray-800 mb-4">Transaction Details</h2>
        <div className="flex justify-between py-2">
          <span className="font-semibold text-gray-600">Transaction ID:</span>
          <span className="text-gray-800">{data?.id}</span>
        </div>
        <div className="flex justify-between py-2">
          <span className="font-semibold text-gray-600">Type:</span>
          <span className="text-gray-800">{
            data?.type ? transactionType.find((status) => Number(status.value) === data?.type)?.label : "N/A"
          }</span>
        </div>
        <div className="flex justify-between py-2">
          <span className="font-semibold text-gray-600">Status:</span>
          <span className="text-gray-800">{
            data?.status ? transactionStatus.find((status) => Number(status.value) === data?.status)?.label : "N/A"
          }</span>
        </div>
        <div className="flex justify-between py-2">
          <span className="font-semibold text-gray-600">Transaction Date:</span>
          <span className="text-gray-800">{formattedDate}</span>
        </div>
        <div className="flex justify-between py-2">
          <span className="font-semibold text-gray-600">Amount:</span>
          <span className="text-gray-800">{
            data?.amount ? formatToVND(data?.amount) : "N/A"
          }</span>
        </div>
        <div className="flex justify-between py-2">
          <span className="font-semibold text-gray-600">User ID:</span>
          <span className="text-gray-800">{data?.userId}</span>
        </div>
        <div className="flex justify-between py-2">
          <span className="font-semibold text-gray-600">Name:</span>
          <div className="flex items-center gap-2">
            <img src={user?.avatarLink} alt="avatar" className="w-8 h-8 rounded-full" />
            <span className="text-gray-800">{`${user?.name}(${user?.username})`}</span>
          </div>
        </div>
      </div>
      <div className="flex items-center gap-4 m-5">
        <Button onClick={() => handleUpdateStatus(2)}>Complete</Button>
        <Button
          variant={"destructive"}
          onClick={() => handleUpdateStatus(3)}>Cancel</Button>
      </div>
    </div>
  )
}

export default TransactionDetail