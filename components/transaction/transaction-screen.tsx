'use client';
import BreadCrumb from '@/components/breadcrumb';
import { columns } from '@/components/tables/transaction-tables/columns';
import { Separator } from '@/components/ui/separator';
import { useTransactionPaginated } from '@/hooks/useTransaction';
import { TransactionTable } from '../tables/transaction-tables/transaction-table';
import { Heading } from '../ui/heading';

const breadcrumbItems = [{ title: 'Transaction', link: '/dashboard/transaction' }];

type paramsProps = {
  searchParams: {
    [key: string]: string | string[] | undefined;
  };
};

export default function Transaction({ searchParams }: paramsProps) {
  const page = Number(searchParams.page) || 1;
  const pageLimit = Number(searchParams.limit) || 10;
  const country = searchParams.search || null;


  const { isPending, isError, error, data, isFetching, isPlaceholderData } = useTransactionPaginated({
    page: page,
    size: pageLimit,
    sortBy: 'transactionDate',
    sortOrder: 'DESC',
    keyword: 'type',
    value: '1'
  });

  console.log(data);

  const orders = data?.items || [];
  const totalOrders = data?.totalCount || 0;
  const pageCount = data?.totalPages;

  return (
    <>
      <div className="flex-1 space-y-4  p-4 pt-6 md:p-8">
        <BreadCrumb items={breadcrumbItems} />

        <div className="flex items-start justify-between">
          <Heading
            title={`Transactions (${data?.totalCount || 0})`}
            description="Manage transactions (Client side table functionalities.)"
          />
        </div>
        <Separator />

        <TransactionTable
          searchKey="country"
          pageNo={page}
          columns={columns}
          totalUsers={totalOrders}
          data={orders}
          pageCount={pageCount}
        />
      </div>
    </>
  );
}
