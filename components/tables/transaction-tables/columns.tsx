'use client';
import { transactionStatus, transactionType } from '@/constants/data';
import { cn, formatDate, formatToVND } from '@/lib/utils';
import { ColumnDef } from '@tanstack/react-table';
import { CellAction } from './cell-action';

export const columns: ColumnDef<any>[] = [
  // {
  //   id: 'select',
  //   header: ({ table }) => (
  //     <Checkbox
  //       checked={table.getIsAllPageRowsSelected()}
  //       onCheckedChange={(value) => table.toggleAllPageRowsSelected(!!value)}
  //       aria-label="Select all"
  //     />
  //   ),
  //   cell: ({ row }) => (
  //     <Checkbox
  //       checked={row.getIsSelected()}
  //       onCheckedChange={(value) => row.toggleSelected(!!value)}
  //       aria-label="Select row"
  //     />
  //   ),
  //   enableSorting: false,
  //   enableHiding: false
  // },
  {
    accessorKey: 'id',
    header: 'ID'
  },
  {
    accessorKey: 'type',
    header: 'Type',
    cell: ({ row }) => {
      const type = transactionType.find((type) => Number(type.value) === row.original.type);
      return <span className={cn("text-xs text-white bg-blue-500 px-2 py-1 rounded", {
        'bg-red-500': row.original.status === 1,
        'bg-green-500': row.original.status === 2,
        'bg-blue-500': row.original.status === 3
      })}>{type?.label}</span>;
    }
  },
  {
    accessorKey: 'status',
    header: 'Status',
    cell: ({ row }) => {
      const type = transactionStatus.find((type) => Number(type.value) === row.original.status);
      return <span className={cn("text-xs text-white bg-blue-500 px-2 py-1 rounded", {
        'bg-red-500': row.original.status === 3,
        'bg-green-500': row.original.status === 2,
        'bg-blue-500': row.original.status === 1
      })}>{type?.label}</span>;
    }
  },
  {
    accessorKey: 'amount',
    header: 'Amount',
    cell: ({ row }) => <span>{formatToVND(row.original.amount)}</span>
  },
  {
    accessorKey: 'userId',
    header: 'UserId'
  },
  {
    accessorKey: 'orderId',
    header: 'OrderId'
  },
  {
    accessorKey: 'transactionDate',
    header: 'Transaction Date',
    cell: ({ row }) => <span>{formatDate(row.original.transactionDate)}</span>
  },
  // {
  //   id: 'actions',
  //   cell: ({ row }) => <CellAction data={row.original} />
  // }
];
