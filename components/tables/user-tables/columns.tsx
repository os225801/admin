'use client';
import { role, transactionStatus, transactionType } from '@/constants/data';
import { cn, formatDate, formatToVND } from '@/lib/utils';
import { ColumnDef } from '@tanstack/react-table';
import { CellAction } from './cell-action';

export const columns: ColumnDef<any>[] = [
  {
    accessorKey: 'id',
    header: 'ID'
  },
  {
    accessorKey: 'role_id',
    header: 'Role',
    cell: ({ row }) => {
      const type = role.find((role) => Number(role.value) === row.original.role_id);
      return <span className={cn("text-xs text-white bg-blue-500 px-2 py-1 rounded", {
        'bg-red-500': row.original.status === 1,
        'bg-green-500': row.original.status === 2,
        'bg-blue-500': row.original.status === 3
      })}>{type?.label}</span>;
    }
  },
  {
    accessorKey: 'user_id',
    header: 'UserId',
  },
  {
    accessorKey: 'expiry_date',
    header: 'Expiry Date',
    cell: ({ row }) => <span>{formatDate(row.original.expiry_date)}</span>
  },
  {
    accessorKey: 'user.username',
    header: 'Username',
  },
  // {
  //   accessorKey: 'transactionDate',
  //   header: 'Transaction Date',
  //   cell: ({ row }) => <span>{formatDate(row.original.transactionDate)}</span>
  // },
  // {
  //   id: 'actions',
  //   cell: ({ row }) => <CellAction data={row.original} />
  // }
];
