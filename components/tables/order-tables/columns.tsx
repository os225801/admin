'use client';
import { orderStatus } from '@/constants/data';
import { cn, formatDate, formatToVND } from '@/lib/utils';
import { ColumnDef } from '@tanstack/react-table';

export const columns: ColumnDef<any>[] = [
  // {
  //   id: 'select',
  //   header: ({ table }) => (
  //     <Checkbox
  //       checked={table.getIsAllPageRowsSelected()}
  //       onCheckedChange={(value) => table.toggleAllPageRowsSelected(!!value)}
  //       aria-label="Select all"
  //     />
  //   ),
  //   cell: ({ row }) => (
  //     <Checkbox
  //       checked={row.getIsSelected()}
  //       onCheckedChange={(value) => row.toggleSelected(!!value)}
  //       aria-label="Select row"
  //     />
  //   ),
  //   enableSorting: false,
  //   enableHiding: false
  // },
  {
    accessorKey: 'id',
    header: 'Id'
  },
  {
    accessorKey: 'orderName',
    header: 'Order Name'
  },
  {
    accessorKey: 'amount',
    header: 'Amount',
    cell: ({ row }) => <span>{formatToVND(row.original.amount)}</span>
  },
  {
    accessorKey: 'orderDate',
    header: 'Order Date',
    cell: ({ row }) => <span>{formatDate(row.original.orderDate)}</span>
  },
  {
    accessorKey: 'status',
    header: 'Status',
    cell: ({ row }) => {
      const status = orderStatus.find((status) => Number(status.value) === row.original.status);
      return <span className={cn("text-xs text-white bg-blue-500 px-2 py-1 rounded", {
        'bg-red-500': row.original.status === 3,
        'bg-green-500': row.original.status === 2,
        'bg-blue-500': row.original.status === 1
      })}>{status?.label}</span>;
    }
  },
  {
    accessorKey: 'buyerId',
    header: 'Buyer Id'
  },
  {
    accessorKey: 'sellerId',
    header: 'Seller Id',
  },
  // {
  //   accessorKey: 'artworkId',
  //   header: 'Artwork Id'
  // },
  // {
  //   id: 'actions',
  //   cell: ({ row }) => <CellAction data={row.original} />
  // }
];
