'use client';
import BreadCrumb from '@/components/breadcrumb';
import { columns } from '@/components/tables/user-tables/columns';
import { Separator } from '@/components/ui/separator';
import { Heading } from '@/components/ui/heading';
import { useLoggingUser } from '@/hooks/useProfile';
import { UserTable } from '@/components/tables/user-tables/user-table';


const breadcrumbItems = [{ title: 'User', link: '/dashboard/user' }];

type paramsProps = {
  searchParams: {
    [key: string]: string | string[] | undefined;
  };
};

export default function User({ searchParams }: paramsProps) {
  const page = Number(searchParams.page) || 1;
  const pageLimit = Number(searchParams.limit) || 10;
  const country = searchParams.search || null;


  const { isPending, isError, error, data, isFetching, isPlaceholderData } = useLoggingUser({
    page: page,
    size: pageLimit,
    sortBy: 'expiryDate',
    sortOrder: 'DESC',
  });

  console.log(data);

  const orders = data?.items || [];
  const totalOrders = data?.totalCount || 0;
  const pageCount = data?.totalPages;

  return (
    <>
      <div className="flex-1 space-y-4  p-4 pt-6 md:p-8">
        <BreadCrumb items={breadcrumbItems} />

        <div className="flex items-start justify-between">
          <Heading
            title={`Users (${data?.totalCount || 0})`}
            description="Manage users (Client side table functionalities.)"
          />
        </div>
        <Separator />

        <UserTable
          searchKey="country"
          pageNo={page}
          columns={columns}
          totalUsers={totalOrders}
          data={orders}
          pageCount={pageCount}
        />
      </div>
    </>
  );
}
