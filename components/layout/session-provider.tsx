'use client'
import { useRouter, useSearchParams } from 'next/navigation'
// context/SessionContext.js
import { createContext, useContext, useEffect, useState } from 'react'

const SessionContext = createContext<{
  userId: string | null
  role: string | null
  userToken: string | null
} | null>(null)

export const SessionProvider = ({ children }: { children: React.ReactNode }) => {
  const [session, setSession] = useState<{
    userId: string | null
    role: string | null
    userToken: string | null
  }>({
    userId: null,
    role: null,
    userToken: null,
  })

  const router = useRouter()
  const searchParams = useSearchParams()

  useEffect(() => {
    const userId = searchParams.get('userId')
    const role = searchParams.get('role')
    const userToken = searchParams.get('userToken')

    if (userId && role && userToken) {
      // Store in session
      ; (async () => {
        sessionStorage.setItem('userId', userId)
        sessionStorage.setItem('role', role)
        sessionStorage.setItem('userToken', userToken)

        // Optionally, redirect to clean the URL
        window.location.href = window.location.origin
      })()
    }
  }, [searchParams])


  useEffect(() => {
    const userId = sessionStorage.getItem('userId')
    const role = sessionStorage.getItem('role')
    const userToken = sessionStorage.getItem('userToken')

    if (userId && role && userToken) {
      setSession({ userId, role, userToken })
    }

    if (userId && role === '3' && userToken) {
      setSession({ userId, role, userToken })
      router.push('/dashboard')
    } else {
      router.push('/')
    }
  }, [])

  return <SessionContext.Provider value={session}>{children}</SessionContext.Provider>
}

export const useSession = () => {
  return useContext(SessionContext)
}
