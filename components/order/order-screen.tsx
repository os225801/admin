'use client';
import BreadCrumb from '@/components/breadcrumb';
import { columns } from '@/components/tables/order-tables/columns';
import { OrderTable } from '@/components/tables/order-tables/order-table';
import { buttonVariants } from '@/components/ui/button';
import { Separator } from '@/components/ui/separator';
import { useOrderPaginated } from '@/hooks/useOrder';
import { cn } from '@/lib/utils';
import { Plus } from 'lucide-react';
import Link from 'next/link';
import { Heading } from '../ui/heading';

const breadcrumbItems = [{ title: 'Order', link: '/dashboard/order' }];

type paramsProps = {
  searchParams: {
    [key: string]: string | string[] | undefined;
  };
};

export default function Order({ searchParams }: paramsProps) {
  const page = Number(searchParams.page) || 1;
  const pageLimit = Number(searchParams.limit) || 10;
  const country = searchParams.search || null;
  // const offset = (page - 1) * pageLimit;

  // const res = await fetch(
  //   `https://api.slingacademy.com/v1/sample-data/users?offset=${offset}&limit=${pageLimit}` +
  //   (country ? `&search=${country}` : '')
  // );

  const { data } = useOrderPaginated({
    page: page,
    size: pageLimit,
    sortBy: 'status',
    sortOrder: 'DESC',
    status: 1
  });

  console.log(data);

  const orders = data?.items || [];
  const totalOrders = data?.totalCount || 0;
  const pageCount = data?.totalPages;

  return (
    <>
      <div className="flex-1 space-y-4  p-4 pt-6 md:p-8">
        <BreadCrumb items={breadcrumbItems} />

        <div className="flex items-start justify-between">
          <Heading
            title={`Orders (${totalOrders || 0})`}
            description="Manage order (Server side table functionalities.)"
          />

          {/* <Link
            href={'/dashboard/order/new'}
            className={cn(buttonVariants({ variant: 'default' }))}
          >
            <Plus className="mr-2 h-4 w-4" /> Add New
          </Link> */}
        </div>
        <Separator />

        <OrderTable
          searchKey="country"
          pageNo={page}
          columns={columns}
          totalUsers={totalOrders}
          data={orders}
          pageCount={pageCount}
        />
      </div>
    </>
  );
}
