"use client"
import { orderStatus } from '@/constants/data'
import { useOrderDetail, useUpdateOrderStatus } from '@/hooks/useOrder'
import { useBuyerId, useSellerId } from '@/lib/store'
import { useParams } from 'next/navigation'
import { Button } from '../ui/button'
import { formatToVND } from '@/lib/utils'
import { useProfile } from '@/hooks/useProfile'

const OrderDetail = () => {
  const { orderId } = useParams()
  const { data } = useOrderDetail(String(orderId))
  const buyerId = useBuyerId()
  const sellerId = useSellerId()
  const { data: buyer } = useProfile({ params: { id: buyerId } })
  const { data: seller } = useProfile({ params: { id: sellerId } })

  const { updateOrder } = useUpdateOrderStatus()

  const handleUpdateStatus = async (status: number) => {
    await updateOrder({ orderId: String(orderId), status, userId: buyerId })
  }

  const formattedDate = new Date(data?.orderDate).toLocaleString();

  return (
    <div>
      <div className="border border-gray-300 rounded-lg p-6 w-full mx-auto mt-10 font-sans">
        <h2 className="text-2xl font-bold text-center text-gray-800 mb-4">Order Details</h2>
        <div className="flex justify-between py-2">
          <span className="font-semibold text-gray-600">Order ID:</span>
          <span className="text-gray-800">{data?.id}</span>
        </div>
        <div className="flex justify-between py-2">
          <span className="font-semibold text-gray-600">Order Date:</span>
          <span className="text-gray-800">{formattedDate}</span>
        </div>
        <div className="flex justify-between py-2">
          <span className="font-semibold text-gray-600">Amount:</span>
          <span className="text-gray-800">{
            data?.amount ? formatToVND(data?.amount) : "N/A"
          }</span>
        </div>
        <div className="flex justify-between py-2">
          <span className="font-semibold text-gray-600">Status:</span>
          <span className="text-gray-800">{
            data?.status ? orderStatus.find((status) => Number(status.value) === data?.status)?.label : "N/A"
          }</span>
        </div>
        <div className="flex justify-between py-2">
          <span className="font-semibold text-gray-600">Buyer ID:</span>
          <span className="text-gray-800">{data?.buyerId}</span>
        </div>
        <div className="flex justify-between py-2">
          <span className="font-semibold text-gray-600">Name:</span>
          <div className="flex items-center gap-2">
            <img src={buyer?.avatarLink} alt="avatar" className="w-8 h-8 rounded-full" />
            <span className="text-gray-800">{`${buyer?.name}(${buyer?.username})`}</span>
          </div>
        </div>
        <div className="flex justify-between py-2">
          <span className="font-semibold text-gray-600">Seller ID:</span>
          <span className="text-gray-800">{data?.sellerId}</span>
        </div>
        <div className="flex justify-between py-2">
          <span className="font-semibold text-gray-600">Name:</span>
          <div className="flex items-center gap-2">
            <img src={seller?.avatarLink} alt="avatar" className="w-8 h-8 rounded-full" />
            <span className="text-gray-800">{`${seller?.name}(${seller?.username})`}</span>
          </div>
        </div>
        <div className="flex justify-between py-2">
          <span className="font-semibold text-gray-600">Artwork ID:</span>
          <span className="text-gray-800">{data?.artworkId}</span>
        </div>
        <div className="flex justify-between py-2">
          <span className="font-semibold text-gray-600">Transfer Content:</span>
          <span className="text-gray-800">{data?.transferContent || 'N/A'}</span>
        </div>
        <div className="flex justify-center py-2">
          <img src={data?.image} alt="Artwork" className="rounded-lg shadow-lg" />
        </div>
      </div>
      <div className="flex items-center gap-4 my-5">
        <Button
          onClick={() => handleUpdateStatus(2)}
        >Complete</Button>
        <Button
          onClick={() => handleUpdateStatus(3)}
          variant={"destructive"}
        >Cancel</Button>
      </div>
    </div>
  )
}

export default OrderDetail