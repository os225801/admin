'use client';


import Link from 'next/link';
import { Icons } from './icons';
import { Button } from './ui/button';

export default function GoogleSignInButton() {
  let currentUrl = ''
  if (typeof window !== 'undefined') {
    currentUrl = window.location.href
  }

  return (
    <Link className="w-full" href={`${process.env.NEXT_PUBLIC_BASE_URL}/login?current-url=${currentUrl}`}>
      <Button
        className="w-full"
        variant="outline"
        type="button"
      >
        <Icons.logo className="mr-2 h-4 w-4" />
        Continue with Google
      </Button>
    </Link>
  );
}
