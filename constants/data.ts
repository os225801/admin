import { NavItem } from '@/types';

export const navItems: NavItem[] = [
  {
    title: 'Dashboard',
    href: '/dashboard',
    icon: 'dashboard',
    label: 'Dashboard'
  },
  {
    title: 'User',
    href: '/dashboard/user',
    icon: 'user',
    label: 'user'
  },
  {
    title: 'Transaction',
    href: '/dashboard/transaction',
    icon: 'billing',
    label: 'transaction'
  },
  {
    title: 'Order',
    href: '/dashboard/order',
    icon: 'employee',
    label: 'order'
  }
  // {
  //   title: 'Profile',
  //   href: '/dashboard/profile',
  //   icon: 'profile',
  //   label: 'profile'
  // },
  // {
  //   title: 'Kanban',
  //   href: '/dashboard/kanban',
  //   icon: 'kanban',
  //   label: 'kanban'
  // },
  // {
  //   title: 'Login',
  //   href: '/',
  //   icon: 'login',
  //   label: 'login'
  // }
];

export const role = [
  { label: 'Admin', value: '1' },
  { label: 'User', value: '2' },
  { label: 'Moderator', value: '3' }
];

export const transactionType = [
  { label: 'Buy', value: '1' },
  { label: 'Receive', value: '2' },
  { label: 'Withdraw', value: '3' }
];

export const transactionStatus = [
  { label: 'Pending', value: '1' },
  { label: 'Success', value: '2' },
  { label: 'Failed', value: '3' }
];

export const orderStatus = [
  { label: 'Pending', value: '1' },
  { label: 'Success', value: '2' },
  { label: 'Failed', value: '3' }
];
